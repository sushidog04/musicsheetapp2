//
//  SSScore.h
//  SeeScoreLib
//
//  Copyright (c) 2015 Dolphin Computing Ltd. All rights reserved.
//
// No warranty is made as to the suitability of this for any purpose
//

/*!
 @header	SSScore.h
 @abstract	SSScore is the main interface to SeeScore.
 */

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import "SSTargetLocation.h"
#import "SSDirectionType.h"
#import "SSEditType.h"

#include "sscore.h"
#include "sscore_contents.h"
#include "sscore_playdata.h"
#include "sscore_edit.h"
#include "sscore_contents.h"

@class SSSystem;
@class SSGraphics;
@class SSComponent;
@class SSTimedItem;
@class SSDirectionType;
@class SSDisplayedItem;
@class SSEditItem;
@class SSLyric;

/*!
 @protocol ScoreChangeHandler
 @abstract interface for a handler which is automatically invoked when a change is made to the score
 */
@protocol ScoreChangeHandler
/*!
 @method change
 @abstract automatic notification of change to ScoreChangeHandler-derived class registered with addChangeHandler
 there are methods to find generic differences between prevstate and newstate so the ui update can be optimised
 eg if only a single bar is changed then you may be able to suppress updates to systems which don't contain the bar (but 
 if the bar becomes wider then you may need to relayout all systems after the changed bar
 @param prevstate the previous state of the score before the change
 @param newstate the new state of the score after the change
 @param reason see enum sscore_state_changeReason
 */
-(void)change:(sscore_state_container * _Nonnull)prevstate newstate:(sscore_state_container * _Nonnull)newstate reason:(/*enum sscore_state_changeReason*/int)reason;
@end

/*!
 @interface SSLoadOptions
 @abstract options for loading the XML file is a parameter to scoreWithXMLData and scoreWithXMLFile
 */
@interface SSLoadOptions : NSObject

/*!
 @property key
 @abstract set to point to the key which allows access to locked features
 */
@property const sscore_libkeytype * _Nullable key;

/*!
 @property compressed
 @abstract set to load compressed .mxl data (redundant when loading a file as it detects the .mxl file extension)
 */
@property bool compressed;

/*!
 @property checkxml
 @abstract set to return information about non-fatal faults (warnings) in the XML file in SSLoadError.warnings
 */
@property bool checkxml;

/*!
 @property suppressAutoBeaming
 @abstract set to ensure it does not automatically generate beam elements, which it only does when there are no beam elements in the score
 */
@property bool suppressAutoBeaming;

/*!
 @method initWithKey
 */
-(instancetype  _Nonnull)initWithKey:(const sscore_libkeytype * _Nullable)key;

@end

/*!
 @interface SSLoadWarning
 @abstract information about a warning ie a non-fatal error in the xml file
 */
@interface SSLoadWarning : NSObject

/*!
 @property warning a warning value
 */
@property (readonly) enum sscore_warning warning;

/*!
 @property partIndex part index (-1 for all parts)
 */
@property (readonly)  int partIndex;

/*!
 @property barIndex bar index (-1 for all bars)
 */
@property (readonly)  int barIndex;

/*!
 @property element
 */
@property (readonly)  enum sscore_element_type element;

/*!
 @property item handle
 */
@property (readonly)  sscore_item_handle item_h;

/*!
 @property priority
 */
@property (readonly) enum sscore_warning_priority priority;

/*!
 @property toString
 @abstract printable string describing this warning
 */
@property (readonly) NSString * _Nonnull toString;

//private
-(instancetype _Nonnull)initWith:(enum sscore_warning)w prio:(enum sscore_warning_priority)prio part:(int)partindex bar:(int)barindex element:(enum sscore_element_type)element item:(sscore_item_handle)item_h;

@end


/*!
 @interface SSLoadError
 @abstract information about any error, and all warnings
 */
@interface SSLoadError : NSObject

/*!
 @property err any error on load
 */
@property (readonly) enum sscore_error err;

/*!
 @property line the line number in the xml file of the (first) error (0 if none)
 */
@property (readonly)  int line;

/*!
 @property col the file column in the line (0 if none)
 */
@property (readonly)  int col;

/*!
 @property text any more information on the error
 */
@property (readonly)  NSString * _Nonnull text;

/*!
 @property warnings any warnings on load if SSLoadOptions.checkxml was set
 */
@property (readonly)  NSArray<SSLoadWarning*> * _Nonnull warnings;

//private
-(instancetype _Nonnull)initWith:(sscore_loaderror)err;

@end


/*!
 @interface SSPartName
 @abstract full and abbreviated names for a part defined in XML
 */
@interface SSPartName : NSObject

/*!
 @property abbreviated_name
 @abstract the abbreviated part name
 */
@property NSString * _Nonnull abbreviated_name;

/*!
 @property full_name
 @abstract the full part name
 */
@property NSString * _Nonnull full_name;

/*!
 @property idString
 @abstract the id eg "P1"
 */
@property NSString * _Nonnull idString;

/*!
 @property item_h
 @abstract the unique handle
 */
@property sscore_item_handle item_h;

@end



/*!
 @interface SSBarGroup
 @abstract detailed information about a group of items in a bar
 */
@interface SSBarGroup : NSObject

/*!
 @property partindex
 @abstract the 0-based index of the part containing this group
 */
@property int partindex;

/*!
 @property barindex
 @abstract the 0-based index of the bar containing this group
 */
@property int barindex;

/*!
 @property items
 @abstract the array of all items in the bar
 */
@property NSArray<SSDisplayedItem*> * _Nonnull items;

/*!
 @property divisions
 @abstract the divisions per quarter note (crotchet) in the bar
 */
@property int divisions;

/*!
 @property divisions_in_bar
 @abstract the total number of divisions in the bar
 */
@property int divisions_in_bar;

@end


/*!
 @interface SSHeader
 @abstract the MusicXML header containing info about the score - title, composer etc.
 */
@interface SSHeader : NSObject

/*!
 @property work_number
 @abstract the work_number in the MusicXML header
 */
@property  NSString * _Nonnull work_number;

/*!
 @property work_title
 @abstract the work_title in the MusicXML header
 */
@property  NSString * _Nonnull work_title;

/*!
 @property movement_number
 @abstract the movement_number in the MusicXML header
 */
@property  NSString * _Nonnull movement_number;

/*!
 @property movement_title
 @abstract the movement_title in the MusicXML header
 */
@property  NSString * _Nonnull movement_title;

/*!
 @property composer
 @abstract the composer of this work in the MusicXML header
 */
@property  NSString * _Nonnull composer;

/*!
 @property lyricist
 @abstract the lyricist of this work in the MusicXML header
 */
@property  NSString * _Nonnull lyricist;

/*!
 @property arranger
 @abstract the arranger of this work in the MusicXML header
 */
@property  NSString * _Nonnull arranger;

/*!
 @property credit_words
 @abstract array of NSString
 */
@property  NSArray<NSString*> * _Nonnull credit_words;

/*!
 @property parts
 @abstract array of SSPartName
 */
@property  NSArray<SSPartName*> * _Nonnull parts;

/*!
 @method partNameForHandle
 @abstract return the part name for unique handle
 */
-(SSPartName * _Nullable )partNameForHandle:(sscore_item_handle)item_h;

@end


/*!
 @interface SSLayoutOptions
 @abstract options for layout of System(s)
 */
@interface SSLayoutOptions : NSObject

/*!
 @property hideBarNumbers
 @abstract set if the bar numbers should not be displayed
 */
@property bool hideBarNumbers;

/*!
 @property hidePartNames
 @abstract set if the part names should not be displayed
 */
@property bool hidePartNames;

/*!
 @property simplifyHarmonyEnharmonicSpelling
 @abstract set this flag so F-double-sharp appears in a harmony as G
 */
@property bool simplifyHarmonyEnharmonicSpelling;

/*!
 @property ignoreXMLPositions
 @abstract set if the default-x, default-y, relative-x, relative-y values in the XML should be ignored
 */
@property bool ignoreXMLPositions;

/*!
 @property useXMLxLayout
 @abstract set to use default-x values on notes and bar width values
 */
@property bool useXMLxLayout;

/*!
 @property redBeyondBarEnd
 @abstract set to display notes in red if they are beyond the end of the bar as defined by the time signature
 */
@property bool redBeyondBarEnd;

/*!
 @property editMode
 @abstract set for increased staff spacings for edit mode
 */
@property bool editMode;

/*! not supported yet
 */
@property bool expandRepeat;

/*!
 @property interStaffPaddingTenths
 @abstract extra spacing for between staves in multi-staff parts
 */
@property int interStaffPaddingTenths;

@end

/*!
 @interface XMLLineNumbers
 @abstract define start and end line numbers for an element in the XML text
 */
@interface XMLLineNumbers : NSObject

/*!
 @property start
 @abstract start line number for an element in the XML text
 */
@property (readonly) int start;

/*!
 @property end
 @abstract end line number for an element in the XML text
 */
@property (readonly) int end;

@end

/*!
 @interface SSSaveOptions
 @abstract options for file save
 */
@interface SSSaveOptions : NSObject

@property enum sscore_savetype saveType;

@property bool musicXML300_compatible; // set to save in a more compatible way to be read by a MusicXML 3.0 app (excluding ids)

@property bool saveNoteIds; // save note identifiers so that they will be the same when reloaded, (MusicXML 3.1 only feature)

@property bool saveDirectionIds; // save direction identifiers (MusicXML 3.1 only feature)

@property bool saveAllIds; // save all available identifiers (MusicXML 3.1 only feature)

@end


/*!
 @typedef sslayout_callback_block_t
 @abstract definition of the block which is called on adding a system in layoutWithWidth:
 */
typedef bool (^sslayout_callback_block_t)(SSSystem* _Nonnull );


/*!
@interface SSScore
@abstract the main Objective-C interface to SeeScore
@discussion The main class of the SeeScore API, this encapsulates all information about the score loaded from a MusicXML file. You will
 find it more convenient and much simpler to use this interface from your Objective-C app rather than the pure C interface in
 sscore.h etc.
 <p>
 loadXMLFile() or loadXMLData() should be used to load a file and create a SScore object
 <p>
 layout() should be called on a background thread to create a layout, and SSystems are generated sequentially from
 the top and can be added to the display as they are produced, but you should ensure you do any UI operations on the foreground
 thread (using dispatch_after). This is all handled by SSView supplied as part of the sample app
 <p>
 numBars, numParts, getHeader(), getPartNameForPart(), getBarNumberForIndex() all return basic information about the score.
 <p>
 setTranspose() allows you to transpose the score.
 <p>
 Other methods return detailed information about items in the score and require a contents or contents-detail licence.

 */
@interface SSScore : NSObject

/*!
@property rawscore
@abstract the low level C API to the score
 */
@property (readonly) sscore*  _Nonnull rawscore;

/*!
@property numParts
@abstract the total number of parts in the score.
 */
@property (readonly) int numParts;

/*!
@property numBars
@abstract the total number of bars in the score.
 */
@property (readonly) int numBars;

/*!
@property header
@abstract the MusicXML header information
 */
@property (readonly) SSHeader * _Nonnull header;

/*!
@property scoreHasDefinedTempo
@abstract true if there is a metronome or sound tempo defined in the XML
 */
@property (readonly) bool scoreHasDefinedTempo;

/*!
 @property isModified
 @abstract true if the current score is changed from when last loaded or saved
 */
@property (readonly) bool isModified;

/*!
 @property hasUndo
 @abstract true if is possible to undo the last edit
 */
@property (readonly) bool hasUndo;

/*!
 @property hasRedo
 @abstract true if is possible to redo the last undo
 */
@property (readonly) bool hasRedo;

/*!
@method version
@abstract the version of the SeeScore library
@return the version hi.lo
 */
+(sscore_version)version;

/*!
 @method versionString
 @abstract the version of the SeeScore library in the format "SeeScoreLib V2.54"
 @return the version string
 */
+(NSString*  _Nonnull)versionString;

-(instancetype  _Nonnull)init NS_UNAVAILABLE;

/*!
 @method headerFromXMLFile
 @abstract get the MusicXML header information quickly from the file without loading the whole file
 @return the header strings only (empty credits/parts). Return nil on error
 */
+(SSHeader* _Nonnull)headerFromXMLFile:(NSString* _Nonnull)filePath;

/*!
 @method scoreWithXMLData:
 @important note - type of error has been changed from sscore_loaderror to SSLoadError
 @abstract load the XML in-memory data and return a SSScore or null if error
 @discussion When loading mxl compressed data you must set the compressed flag in SSLoadOptions
 Any warnings are returned in err.warnings if SSLoadOptions.checkxml was set
 Swift signature SSScore(XMLData:data : NSData, options: loadOptions : SSLoadOptions, error: err : sscore_loaderror)
 @param data the NSData containing the MusicXML
 @param loadOptions the options for load including the licence key
 @param err returned pointer to a SSLoadError filled with the errors and warnings
 @return the SSScore or NULL if error with the error returned in err
 */
+(SSScore* _Nullable)scoreWithXMLData:(NSData * _Nonnull)data
							  options:(SSLoadOptions* _Nonnull)loadOptions
								error:(SSLoadError * _Nonnull * _Nullable)err;

/*!
 @method scoreWithXMLFile:
 @important note - type of error has been changed from sscore_loaderror to SSLoadError
 @abstract load the XML file and return a SSScore or null if error
 @discussion This uses the .mxl file extension to detect mxl data
 Any warnings are returned in err.warnings if SSLoadOptions.checkxml was set
 Swift signature SSScore(XMLFile: xmlFilePath : String, options: loadOptions : SSLoadOptions, error: err : sscore_loaderror)
 @param filePath the full pathname of the file to load
 @param loadOptions the options for load including the licence key
 @param err returned pointer to a SSLoadError filled with the errors and warnings
 @return the SSScore or NULL if error with the error returned in err
 */
+(SSScore* _Nullable)scoreWithXMLFile:(NSString * _Nonnull)filePath
							  options:(SSLoadOptions* _Nonnull)loadOptions
								error:(SSLoadError * _Nonnull * _Nullable)err;

/*!
 @method scoreWithXMLUrl:
 @abstract load the XML file and return a SSScore or null if error
 @discussion This uses the .mxl file extension to detect mxl data
 Any warnings are returned in err.warnings if SSLoadOptions.checkxml was set
 Swift signature SSScore(XMLUrl: fileUrl : String, options: loadOptions : SSLoadOptions, error: err : sscore_loaderror)
 @param fileUrl the URL of the file to load
 @param loadOptions the options for load including the licence key
 @param err returned pointer to a SSLoadError filled with the errors and warnings
 @return the SSScore or NULL if error with the error returned in err
 */
+(SSScore* _Nullable)scoreWithXMLUrl:(NSURL * _Nonnull)fileUrl
							 options:(SSLoadOptions* _Nonnull)loadOptions
							   error:(SSLoadError * _Nonnull * _Nullable)err;

/*!
 @method extractFileSection:file:numLines:
 @abstract extract a section of a text file around a given line number into a NSString
 @discussion This can be used to display a problem in a filewhich won't load using scoreWith..
 @param filePath the full pathname of the file to load
 @param line the central line number in the extracted section
 @param numLines the number of lines to extract
 */
+(NSString* _Nonnull)extractFileSection:(NSString * _Nonnull)filePath line:(int)line numLines:(int)numLines;

/*!
 @method reportXmlValidationErrorHtml:errorString:line:numLines:
 @abstract report the xml validation error in html format to display in a web view
 @discussion This can be used to display a problem in a file which won't load using scoreWith..
 @param fileUrl the url of the xml file from which the relevant section will be extracted for the report
 @param errorString the validation error from the parser
 @param line the line number of the error, and central line number in the extracted section
 @param numLines the number of lines to extract for context
 */
+(NSString* _Nonnull)reportXmlValidationErrorHtml:(NSURL * _Nonnull)fileUrl errorString:(NSString* _Nonnull)errorString line:(int)line numLines:(int)numLines;

/*!
 @method getXMLWarnings:
 @abstract get list of any problems in the XML
*/
-(NSArray<SSLoadWarning*> * _Nonnull)getXMLWarnings;

/*!
 @method compressFile:
 @abstract compress the file, and return the path of the compressed file in the same directory
 @param filePath the file path which must be of extension .xml
 @return the pathname of the compressed file with extension .mxl, or nil if failed
 */
+(NSString* _Nullable)compressFile:(NSString* _Nonnull)filePath;

/*!
 @method partNameForPart:
 @abstract return the name for the part.
 @param partindex the index of the part [0..numparts-1]
 @return PartName (full + abbreviated)
 */
-(SSPartName* _Nonnull)partNameForPart:(int)partindex;

/*!
 @method instrumentNameForPart:
 @abstract return the name for the instrument for the part.
 @discussion This is often not defined in the XML, in which case the part name is probably the instrument name
 @param partindex the index of the part [0..numparts-1]
 @return instrument name, empty if not defined
 */
-(NSString* _Nonnull)instrumentNameForPart:(int)partindex;

/*!
 @method updateHeader
 @abstract change a field in the header
 @param fieldId defines which field to change
 @param val defines the new value. If empty string the field is removed from the xml
 */
-(void)updateHeader:(enum sscore_header_fieldid)fieldId val:(NSString* _Nonnull)val;

/*!
 @method saveToFile
 @abstract save the score to a MusicXML file
 @discussion this saves safely ie it saves to a tempoarary file and then copies that to the destination only if it succeeds
 @param filePath the pathname of the file to save - if the extension is .mxl the file is automatically compressed after saving
 @return any error
 */
-(enum sscore_error)saveToFile:(NSString* _Nonnull)filePath options:(SSSaveOptions* _Nullable)options;

/*!
 @method saveToURL
 @abstract save the score to a MusicXML file defined by a NSURL
 @param url the url of the file to save
 @return any error
 */
-(enum sscore_error)saveToUrl:(NSURL* _Nonnull)url options:(SSSaveOptions* _Nullable)options;

/*!
 @method saveToData
 @abstract save the score to NSData as required by UIDocument.contents
 @discussion the current implementation writes the compressed data to a temporary file, loads the file as NSData, deletes the file and returns the NSData
 @param options define options for save
 @return NSData (probably to be written to a file)
 */
-(NSData* _Nonnull)saveToData:(SSSaveOptions* _Nullable)options;

/*!
 @method saveToString
 @abstract save the score to a NSString in MusicXML format
 @return the NSString - nil if failed
 */
-(NSString* _Nonnull)saveToString:(SSSaveOptions* _Nullable)options;

/*!
 @method layout1SystemWithContext:
 @abstract Layout a single system with a single part.
 @discussion You specify a start bar index and a width and magnification and it will display
 as many bars as will fit into this width, up to a limit of maxBars if > 0.
 <p>Useful for display of individual parts for part selection.
 @param ctx a graphics context only for measurement of text (eg a bitmap context)
 @param startbarindex the index of the first bar in the system (usually 0)
 @param maxBars the maximum number of bars to include in the system, 0 to fit as  many as possible
 @param width the width to display the system within
 @param max_height the maximum height available to display the system to control truncation. =0 for no truncation
 @param partindex the index of the single part to layout [0..numparts-1]
 @param magnification the scale at which to display this (1.0 for normal size)
 @param layoutOptions layout options
 @return the system
 */
-(SSSystem* _Nonnull)layout1SystemWithContext:(CGContextRef _Nonnull)ctx
							startbar:(int)startbarindex
							 maxBars:(int)maxBars
							   width:(CGFloat)width
						   maxheight:(CGFloat)max_height
								part:(int)partindex
					   magnification:(float)magnification
							 options:(SSLayoutOptions* _Nonnull)layoutOptions;

/*!
 @method layoutWithContext:
 @abstract Layout a set of systems and return them through a callback function.
 @discussion This should be called on a background thread and it will call cb for each system laid out,
 from top to bottom. cb will normally add the system to a list and schedule an update on the
 foreground (gui event dispatch) thread to allow the UI to remain active during concurrent layout.
 cb can return false to abort the layout.
 @param ctx a graphics context only for measurement of text (eg a bitmap context)
 @param width the width available to display the systems in screen coordinates
 @param max_system_height the maximum height available to display each system to control truncation. =0 for no truncation
 @param parts array of NSNumber (boolean), 1 per part, true to include, false to exclude
 @param magnification the scale at which to display this (1.0 is default)
 @param layoutOptions the SSLayoutOptions
 @param callback the callback function to be called for each completed system
 @return any error
 */
-(enum sscore_error)layoutWithContext:(CGContextRef _Nonnull)ctx
								width:(CGFloat)width
							maxheight:(CGFloat)max_system_height
								parts:(NSArray<NSNumber*>* _Nonnull)parts
						magnification:(float)magnification
							  options:(SSLayoutOptions* _Nonnull)layoutOptions
							 callback:(sslayout_callback_block_t _Nonnull)callback;

/*!
 @method layoutWidth:parts:magnification:options:
 @abstract return the minimum width to display the first system of the score at the given magnification
 @param ctx a graphics context only for measurement of text (eg a bitmap context)
 @param parts array of NSNumber (boolean), 1 per part, true to include, false to exclude
 @param magnification the scale at which to display this (1.0 is default)
 @param layoutOptions the SSLayoutOptions
 @return the minimum width
 */
-(float)layoutWidth:(CGContextRef _Nonnull)ctx
			  parts:(NSArray<NSNumber*>* _Nonnull)parts
	  magnification:(float)magnification
			options:(SSLayoutOptions* _Nonnull)layoutOptions;

/*!
 @method barNumberForIndex:
 @abstract Get the bar number (String) given the index.
 @param barindex integer index [0..numBars-1]
 @return the score-defined number String (usually "1" for index 0)
 */
-(NSString* _Nonnull)barNumberForIndex:(int)barindex;

/*!
 The transpose methods
 */

/*!
 @method setTranspose:
 @abstract Set a transposition for the score.
 @discussion Call layout() after calling setTranspose for a new transposed layout.
 <p>Requires the transpose licence.
 NB This does not change the underlying score, but only the view of it
 @param semitones (- for down, + for up)
 */
-(enum sscore_error)setTranspose:(int)semitones;

/*!
 @method transpose
 @abstract get the current transpose value set with setTranspose.
 @return the current transpose
 */
-(int)transpose;

@end


/*!
 @abstract The contents methods are available if we have a contents licence
 */
@interface SSScore (Contents)

/*!
 @method itemForPart:bar:handle:err
 @abstract return detailed information about an item in the score.
 @discussion Requires contents-detail licence.
 @param partindex 0-based part index - 0 is the top part
 @param barindex 0-based bar index
 @param item_h unique id for item eg from SSBarGroup
 @param err pointer to a sscore_error to receive any error
 @return SSTimedItem which can be cast to the specific derived type - NoteItem/DirectionItem etc.
 */
-(SSTimedItem* _Nullable)itemForPart:(int)partindex
					   bar:(int)barindex
					handle:(sscore_item_handle)item_h
					   err:(enum sscore_error* _Nullable)err;

/*!
 @method xmlForPart:bar:handle:err:
 @abstract Return the XML for the item in the part/bar.
 @discussion Requires contents licence.
 @param partindex the 0-based part index - 0 is top
 @param barindex the 0-based bar index
 @param item_h the unique id of the item eg from SSBarGroup
 @param err pointer to a sscore_error to receive any error
 @return the XML as a NSString
 */
-(NSString* _Nonnull)xmlForPart:(int)partindex
				   bar:(int)barindex
				handle:(sscore_item_handle)item_h
				   err:(enum sscore_error* _Nullable)err;

/*!
 @method barContentsForPart:bar:err:
 @abstract Get information about the contents of a particular part/bar.
 @discussion Requires contents-detail licence.
 @param partindex the 0-based part index - 0 is top
 @param barindex the 0-based bar index
 @param err pointer to a sscore_error to receive any error
 @return the SSBarGroup containing an array of SSDisplayedItem.
 To get more information call itemForPart:bar:handle:err: using the item_h field 
 */
-(SSBarGroup* _Nonnull)barContentsForPart:(int)partindex
							 bar:(int)barindex
							 err:(enum sscore_error* _Nullable)err;

/*!
 @method itemsForComponents:
 @abstract Get a list of items in BarGroups from a list of components
 @discussion Requires contents licence
 @param components array of SSComponent from SSSystem.closeFeatures or SSystem.featuresWithin
 @return the array of SSBarGroup containing an array of SSDisplayedItem.
 */
-(NSArray<SSBarGroup*>* _Nonnull)itemsForComponents:(NSArray<SSComponent*>* _Nonnull)components;

/*!
 @method xmlForPart:bar:err:
 @abstract Return the raw XML for this given part/bar index as a NSString.
 @discussion Requires contents-detail licence.
 @param partindex the 0-based part index - 0 is top
 @param barindex the 0-based bar index
 @param err pointer to a sscore_error to receive any error
 @return the XML as a NSString
 */
-(NSString* _Nonnull)xmlForPart:(int)partindex
				   bar:(int)barindex
				   err:(enum sscore_error* _Nullable)err;

/*!
 @method xmlForScore
 @abstract Return the (reconstructed) XML for this entire score as a NSString.
 @discussion Requires contents-detail licence.
 @return the XML as a NSString
*/
-(NSString* _Nonnull)xmlForScore;

/*!
 @method barXMLStartEndLineNumbersForPart:bar:
 @abstract return the start and end line numbers for the bar or nil
 @param partIndex the index of the part containing the item
 @param barIndex the index of the bar containing the item
 @return an object containing the line numbers of the start and end of the measure element in the XML text, or nil if no information available
 */
-(XMLLineNumbers* _Nullable)barXMLStartEndLineNumbersForPart:(int)partIndex bar:(int)barIndex;

/*!
 @method itemXMLStartEndLineNumbersForPart:bar:item:
 @abstract return the start and end line numbers for the item or nil
 @param partIndex the index of the part containing the item
 @param barIndex the index of the bar containing the item
 @param item_h the handle for the item
 @return an object containing the line numbers of the start and end of the element describing this item in the XML text, or nil if no information available
 */
-(XMLLineNumbers* _Nullable)itemXMLStartEndLineNumbersForPart:(int)partIndex bar:(int)barIndex item:(sscore_item_handle)item_h;

/*!
 @method compressXML:sourcePath:destDirectoryPath:
 @abstract compress the xml file at sourcePath returning the path of a mxl file containing the result
 @param sourcePath source file path
 @param destDirectoryPath destination (temporary?) directory path to store the result
 @return the file path of the result file containing the decompressed XML with extension .mxl or nil if failed
 */
+(NSString* _Nullable)compressXML:(NSString* _Nonnull)sourcePath dest:(NSString* _Nonnull)destDirectoryPath;

/*!
 @method decompressMXL:sourcePath:destDirectoryPath:
 @abstract decompress the mxl file at sourcePath returning the name of a xml file containing the result
 @param sourcePath source file path
 @param destDirectoryPath destination (temporary?) directory path to store the result
 @return the file path of the result file containing the decompressed XML with extension .xml or nil if failed
 */
+(NSString* _Nullable)decompressMXL:(NSString* _Nonnull)sourcePath dest:(NSString* _Nonnull)destDirectoryPath;

/*!
 @method barTypeForBar:
 @abstract is the bar a full bar?
 @param barIndex the 0-based bar index
 @return the type of bar (full or partial/anacrusis)
 */
-(enum sscore_bartype_e)barTypeForBar:(int)barIndex;

/*!
 @method timeSigForBar:
 @abstract return any time signature actually defined in a particular bar in the score, or zero if none
 @discussion actualBeatsForBar: is more useful to find the operating time signature in a bar
 @param barIndex the 0-based bar index
 @return the time signature in the bar - return beats = 0 if there is none
 */
-(sscore_timesig)timeSigForBar:(int)barIndex;

/*!
 @method actualBeatsForBar:
 @abstract return the time signature operating in a particular bar in the score
 @param barIndex the 0-based bar index
 @return the time signature in operation in the bar
 */
-(sscore_timesig)actualBeatsForBar:(int)barIndex;

/*!
 @method metronomeForBar:
 @abstract get the metronome if defined in a bar
 @param barIndex the 0-based bar index
 @return any metronome defined in the bar
 */
-(sscore_pd_tempo)metronomeForBar:(int)barIndex;

/*!
 @method tempoAtStart:
 @abstract get the tempo at the start of the score if defined
 @return information about the tempo to use at the start of the piece
 */
-(sscore_pd_tempo)tempoAtStart;

/*!
 @method tempoAtBar:
 @abstract get the tempo at a particular bar
 @param barIndex the 0-based bar index
 @return information about the tempo in the bar
 */
-(sscore_pd_tempo)tempoAtBar:(int)barIndex;

/*!
 @method convertTempoToBpm:
 @abstract convert a tempo value into a beats-per-minute value using the time signature
 @param tempo a tempo eg from tempoAtBar
 @param timesig a time signature eg from timeSigForBar
 @return beats per minute value
 */
-(int)convertTempoToBpm:(sscore_pd_tempo) tempo timeSig:(sscore_timesig)timesig;

/*!
 @method getBarBeats:
 @abstract get information about beats in a bar
 @param barindex the 0-based bar index
 @param bpm the beats per minute value
 @param bartype the type of bar (full or partial)
 @return information about number of beats and the beat time in ms for a bar
 */
-(sscore_pd_barbeats)getBarBeats:(int)barindex bpm:(int)bpm barType:(enum sscore_bartype_e) bartype;

/*!
 @method clefForComponentItem:
 @abstract get information about the clef which applies for the item which this component belongs to
 @param component the component
 @return the type of clef in force at the item containing this component
 */
-(enum sscore_clef_type_e)clefForComponentItem:(SSComponent* _Nonnull)component;

/*!
 @method clefApplyingAtPart:bar:staff:
 @abstract get information about the clef which applies in the part/bar/staff
 @param partIndex the part [0..numParts-1]
 @param barIndex the bar [0..numBars-1]
 @param staffIndex the staff [0..1]
 @return the type of clef in force in the part/bar/staff
 */
-(enum sscore_clef_type_e)clefApplyingAtPart:(int)partIndex bar:(int)barIndex staff:(int)staffIndex;

/*!
 @method keyFifthsApplyingAtPart:bar:staff:
 @abstract get information about the key signature which applies in the part/bar/staff
 @param partIndex the part [0..numParts-1]
 @param barIndex the bar [0..numBars-1]
 @param staffIndex the staff [0..1]
 @return the key fifths in force in the part/bar/staff
 */
-(int)keyFifthsApplyingAtPart:(int)partIndex bar:(int)barIndex staff:(int)staffIndex;

/*!
 @method timeSigApplyingAtPart:bar:staff:
 @abstract get information about the time signature which applies in the part/bar/staff
 @param partIndex the part [0..numParts-1]
 @param barIndex the bar [0..numBars-1]
 @param staffIndex the staff [0..1]
 @return the time signature in force in the part/bar/staff
 */
-(sscore_timesig_detail)timeSigApplyingAtPart:(int)partIndex bar:(int)barIndex staff:(int)staffIndex;

/*!
 @method hasLyricsInPart:staffIndex:
 @abstract true if the part/staff has lyrics
 @param partIndex the part [0..numParts-1]
 @param staffIndex the staff [0..1]
 @return true if the part/staff has lyrics
 */
-(bool)hasLyricsInPart:(int)partIndex staffIndex:(int)staffIndex;

/*!
 @method lyricForComponent:
 @abstract find the lyric from a layout component created from it
 @param lyricComponent the component
 @return the lyric
 */
-(SSLyric* _Nullable)lyricForComponent:(SSComponent* _Nonnull)lyricComponent;

/*!
 @method numLyricLinesInPart:staffIndex:
 @abstract get the number of lyric lines in the part
 @param partIndex the 0-based part index
 @return the number of lyric lines in the part
 */
-(int)numLyricLinesInPart:(int)partIndex staffIndex:(int)staffIndex;

/*!
 @method getLyricsInPart:staffIndex:lyricLineIndex:
 @abstract get the lyrics in the part
 @discussion Requires contents licence.
 @param partIndex the 0-based part index
 @return the line of lyrics in the part
 */
-(NSString* _Nonnull)getLyricsInPart:(int)partIndex staffIndex:(int)staffIndex lyricLineIndex:(int)lyricLineIndex;

/*!
 @method firstLyricInPart:staffIndex:lyricLineindex:
 @abstract return information about the lyric on the first note in the part
 @discussion Requires contents licence.
 @param partIndex the 0-based part index
 @param staffIndex the 0-based staff index
 @param lyricLineIndex the 0-based lyric line index - 0 is top line of lyrics in part
 @return information about the first lyric in the part.
 */
-(SSLyric* _Nullable)firstLyricInPart:(int)partIndex staffIndex:(int)staffIndex lyricLineIndex:(int)lyricLineIndex;

/*!
 @method nextLyric:
 @abstract return information about the next lyric after previous in the same part
 @discussion Requires contents licence.
 @param previous the current lyric
 @return information about the next lyric in the part after previous. return nil after last lyric in part
 */
-(SSLyric* _Nullable)nextLyric:(SSLyric* _Nonnull)previous;

/*!
 @method textBBWithContext:text:type:
 @abstract return bounding box for text
 @param ctx the CGContextRef
 @param text the text
 @param textType the text type
 @return the bounding box
 */
-(CGRect)textBBWithContext:(CGContextRef _Nonnull)ctx text:(NSString* _Nonnull)text type:(enum sscore_edit_textType)textType;

/*!
 @method drawTextWithContext:text:type:bottomLeft:scale:colour:
 @abstract draw the text
 @param ctx the CGContextRef
 @param text the text
 @param textType the text type
 @param bottomLeft the bottom left point
 @param scale the scale
 @param colour the colour
 */
-(void)drawTextWithContext:(CGContextRef _Nonnull)ctx text:(NSString* _Nonnull)text type:(enum sscore_edit_textType)textType bottomLeft:(CGPoint)bottomLeft scale:(float)scale colour:(CGColorRef _Nonnull)colour;

@end

/*!
 The information required for playing the score is available through the SSPData class
 (only if a playdata licence is available)
 */

/*!
 @abstract editing methods
 @discussion each edit creates a new state and adds it to a list.
 The change handler is called with new and old state on each state change.
 Undo and redo change the current index into a list of states
 All methods require either the annotate or edit licences
*/
@interface SSScore(Edit)

/*!
 @method undo
 @abstract undo the last edit
 @discussion property hasUndo is true if this can undo
 */
-(void)undo;

/*!
 @method redo
 @abstract redo the last undone edit
 @discussion property hasRedo is true if this can redo
 */
-(void)redo;

/*!
 @method setPartName:text:
 @warning EDITING API IN DEVELOPMENT
 @abstract set the part name of the given part
 @discussion Requires edit licence.
 @param partIndex the 0-based part index
 @param partName the name of the part (usually an instrument name eg "Piano", "Voice") to display at the left of the first system
 @param abbrevName the abbreviated version of the name to use at the left of subsequent systems, may be empty
 @return true if success
 */
-(bool)setPartName:(int)partIndex partName:(NSString* _Nonnull)partName abbrev:(NSString* _Nonnull)abbrevName;

/*!
 @method isValidInsertTarget:forType:
 @warning EDITING API IN DEVELOPMENT
 @abstract is this a valid location for the new item to be inserted?
 @param target the target position in the score for the item
 @param itemType the type of the new item to be inserted
 @return true if a new item of type itemType can be inserted at target
 */
-(bool)isValidInsertTarget:(SSTargetLocation * _Nonnull)target forType:(SSEditType* _Nonnull)itemType;

/*!
 @method isValidMultiInsertTargetLeft:right:forType:
 @warning EDITING API IN DEVELOPMENT
 @abstract is this a valid pair of locations for the new multiple item (eg slur, tied, wedge, repeat barlines) to be inserted?
 @param target_left the left target position in the score for the item
 @param target_right the right target position in the score for the item
 @param itemType the type of the new item to be inserted
 @return true if a new item of multiple type itemType can be inserted at target_left,target_right
 */
-(bool)isValidMultiInsertTargetLeft:(SSTargetLocation * _Nonnull)target_left right:(SSTargetLocation * _Nonnull)target_right forType:(SSEditType* _Nonnull)itemType;

/*!
 @method isValidReinsertTarget
 @warning EDITING API IN DEVELOPMENT
 @abstract is this a valid location for the existing item to be reinserted (ie relocated)?
 @param target the target position in the score for the item
 @param item the existing item to be reinserted
 @return true if item can be reinserted at target
 */
-(bool)isValidReinsertTarget:(SSTargetLocation * _Nonnull)target forItem:(SSEditItem* _Nonnull)item;

/*!
 @method insertIsProvisory:at:
 @warning EDITING API IN DEVELOPMENT
 @abstract return true if this inserted item is not strictly required at this target, eg for a cautionary accidental
 @discussion Requires edit licence.
 @param itemType the type of the item
 @param target the logical insertion place from nearestInsertTargetFor:
 @return true if this item is not strictly necessary at this location, but can be inserted if required
 */
-(bool)insertIsProvisory:(SSEditType * _Nonnull)itemType at:(SSTargetLocation* _Nonnull)target;

/*!
 @method canAddTiedToNotehead
 @warning EDITING API IN DEVELOPMENT
 @abstract is there a suitable note to the right which can be tied to the notehead defined by target
 @param target the target
 @return true if a valid tied pair of elements can be added to the given notehead and one immediately to the right with the same pitch
 */
-(bool)canAddTiedToNotehead:(SSTargetLocation* _Nonnull)target;

/*!
 @method addTiedToNotehead
 @warning EDITING API IN DEVELOPMENT
 @discussion Requires edit licence.
 @abstract add a tied pair if there is a suitable note to the right which can be tied to the notehead defined by target
 @param target the target
 @return true if added pair successfully, false if not possible, with no change to score
 */
-(bool)addTiedToNotehead:(SSTargetLocation* _Nonnull)target;

/*!
 @method singleChoiceBeamToNote:
 @warning EDITING API IN DEVELOPMENT
 @abstract if there is a single neighbouring note that can be beamed to a note defined by target return the id of the note
 @param target defines the note to be beamed
 @return a valid note target if one could be found, else return nil
 */
-(SSTargetLocation* _Nullable)singleChoiceBeamToNote:(SSTargetLocation* _Nonnull)target;

/*!
 @method insertClef:omrEdit:at:
 @warning EDITING API IN DEVELOPMENT
 @discussion Requires edit licence.
 @abstract insert a clef at the given target, replacing any clef existing at the target
 @param itemType the type of clef
 @param omrEdit true if we are correcting OMR (Optical Music Recognition) errors
 (ie if the XML has been generated from an image or pdf) so notes remain anchored to the same position
 on the staff when changing the clef, and accidentals are unchanged when changing the key
 @param target the target
 @return true if added successfully, false if not possible, with no change to score
 */
-(bool)insertClef:(SSEditType * _Nonnull)itemType
		  omrEdit:(bool)omrEdit
			   at:(SSTargetLocation* _Nonnull)target;

/*!
 @method insertDirectionWords:at:where:fontInfo:
 @warning EDITING API IN DEVELOPMENT
 @discussion Requires edit licence.
 @abstract attempt to insert the given text as &lt;direction&gt&lt;direction-type&gt&lt;words&gt in the score at the target location, above the staff.
 @param words the text to insert
 @param target the insert position
 @param finfo the font size and style information or nil
 @return true if insert succeeded
 */
-(bool)insertDirectionWords:(NSString* _Nonnull)words
						 at:(SSTargetLocation* _Nonnull)target
				   fontInfo:(const sscore_edit_fontInfo* _Nullable)finfo;

/*!
 @method insertDirectionWords:atNote:where:fontInfo:
 @warning EDITING API IN DEVELOPMENT
 @discussion Requires edit licence.
 @abstract attempt to insert the given text as &lt;direction&gt&lt;direction-type&gt&lt;words&gt in the score immediately before the given note, above the staff.
 @param words the text to insert
 @param noteComponent the note before which the direction should be inserted
 @param finfo the font size and style information or nil
 @return true if insert succeeded
 */
-(bool)insertDirectionWords:(NSString* _Nonnull)words
					 atNote:(SSComponent * _Nonnull)noteComponent
				   fontInfo:(const sscore_edit_fontInfo* _Nullable)finfo;

/*!
 @method insertLyric:at:
 @warning EDITING API IN DEVELOPMENT
 @discussion Requires edit licence.
 @abstract attempt to insert the given text as a lyric in the score.
 @param lyricText the text to insert
 @param target the insert position
 @return true if insert succeeded
 */
-(bool)insertLyric:(NSString* _Nonnull)lyricText
				at:(SSTargetLocation* _Nonnull)target;

/*!
 @method lyricAtTarget:lyricLine:
 @warning EDITING API IN DEVELOPMENT
 @discussion Requires contents licence.
 @abstract get a lyric on the note at target
 @param target a note-defining target
 @param lyricLine 0 for top lyric on note
 @return lyric if lyric found on note at target in lyricline, else nil
 */
-(SSLyric* _Nullable)lyricAtTarget:(SSTargetLocation* _Nonnull)target lyricLine:(int)lyricLine;

/*!
 @method isEditableText:
 @warning EDITING API IN DEVELOPMENT
 @discussion Requires edit licence.
 @abstract true if it is possible to modify text in the item
 @return true if the item text can be modified
 */
-(bool)isEditableText:(SSEditItem* _Nonnull)item;

/*!
 @method isMoveable:
 @warning EDITING API IN DEVELOPMENT
 @discussion Requires edit licence.
 @abstract true if it is possible to move the item (eg note up/down for pitch change or wedge end left/right)
 @return true if the item can be modified by moving
 */
-(bool)isMoveable:(SSEditItem* _Nonnull)item;

/*!
 @method isDeleteable:
 @warning EDITING API IN DEVELOPMENT
 @discussion Requires edit licence.
 @abstract true if deleteItem should succeed
 @return true if the item can be deleted from the score
 */
-(bool)isDeleteable:(SSEditItem* _Nonnull)item;

/*!
 @method tryInsertItem:at:
 @warning EDITING API IN DEVELOPMENT
 @discussion Requires edit licence.
 @abstract attempt to insert the item in the score at the target location, return true if succeeded
 @param info information about the item to insert
 @param target the target location from nearestInsertTarget
 @return true if insert succeeded
 */
-(bool)tryInsertItem:(const sscore_edit_insertInfo* _Nonnull)info at:(SSTargetLocation* _Nonnull)target;

/*!
 @method tryInsertMultiItem:left:right:
 @warning EDITING API IN DEVELOPMENT
 @discussion Requires edit licence.
 @abstract attempt to insert the multiple item (eg slur,tied,wedge) in the score at the left/right target locations, return true if succeeded
 @param info information about the item to insert
 @param target_left the left target location
 @param target_right the right target location
 @return true if insert succeeded
 */
-(bool)tryInsertMultiItem:(const sscore_edit_insertInfo* _Nonnull)info left:(SSTargetLocation* _Nonnull)target_left right:(SSTargetLocation* _Nonnull)target_right;

/*!
 @method tryOffsetItem:offset:
 @warning EDITING API IN DEVELOPMENT
 @discussion Requires edit licence.
 @abstract attempt to offset an item setting default-x,default-y,bezier-x,bezier-y, return true if succeeded
 @param item information the item to move (only slur/tied bezier control points are supported at present)
 @param offset the x,y offset (from SSSystem.bezierOffset:forSystemPos:)
 @return true if offset successfully applied
 */
-(bool)tryOffsetItem:(SSEditItem * _Nonnull)item offset:(CGPoint)offset;

/*!
 @method dragRequiresReinsert:offset:
 @warning EDITING API IN DEVELOPMENT
 @discussion Requires edit licence.
 @abstract from offset determine if we need to call tryReinsertItem
 @param item information the item to move (only wedge can return true at present)
 @param offset the x,y offset (from SSSystem.bezierOffset:forSystemPos:)
 @return true if the drop with the given offset requires a call to tryReinsertItem
 */
-(bool)dragRequiresReinsert:(SSEditItem* _Nonnull)item offset:(CGPoint)offset;

/*!
 @method dragRequiresNewPlacement:offset:
 @warning EDITING API IN DEVELOPMENT
 @discussion Requires edit licence.
 @abstract from offset determine if we need to call tryModifyPlacementForItem
 @param item information the item to move (only wedge can return true at present)
 @param offset the x,y offset (from SSSystem.bezierOffset:forSystemPos:)
 @return true if the drop with the given offset requires a call to tryModifyPlacementForItem
 */
-(bool)dragRequiresNewPlacement:(SSEditItem* _Nonnull)item offset:(CGPoint)offset;

/*!
 @method tryReinsertItem:at:
 @warning EDITING API IN DEVELOPMENT
 @abstract attempt to reinsert the item in the score at the target location, return true if succeeded
 @discussion this is generally used when we drag an existing item in the score
 eg this is used to repitch a note when it is dragged up or down in the staff
 Requires edit licence.
 @param item the item to reinsert
 @param target the target location from nearestInsertTarget
 @return true if reinsert succeeded
 */
-(bool)tryReinsertItem:(SSEditItem* _Nonnull)item at:(SSTargetLocation* _Nonnull)target;

/*!
 @method tryModifyPlacementForItem:at:
 @warning EDITING API IN DEVELOPMENT
 @abstract attempt to change the placement (above or below) of the item, return true if succeeded
 @discussion this is generally used when we drag an existing item in the score
 Requires edit licence.
 @param item the item whose placement is to be changed
 @param target the target location from nearestInsertTarget
 @return true if modification succeeded
 */
-(bool)tryModifyPlacementForItem:(SSEditItem* _Nonnull)item at:(SSTargetLocation* _Nonnull)target;

/*!
 @method deleteItem:
 @warning EDITING API IN DEVELOPMENT
 @discussion Requires edit licence.
 @abstract delete an item from the score
 @discussion get the item from SSSystem
 @param item an editable item in the score
 @return true if it succeeded
 */
-(bool)deleteItem:(SSEditItem* _Nonnull)item;

/*!
 @method insertPart:numStaves:beforePart:
 @warning EDITING API IN DEVELOPMENT
 @discussion Requires edit licence.
 @abstract insert a new empty part in the score. The new part will have numBars empty bars
 @param name the name of the part
 @param numStaves number of staves in the part
 @param beforePartIndex the index [0..] of a part in the score, before which the new part will be inserted. Use numParts to add at the end
 @return true if it succeeded
 */
-(bool)insertPart:(NSString* _Nonnull)name numStaves:(int)numStaves beforePart:(int)beforePartIndex;

/*!
 @method movePart:before:
 @warning EDITING API IN DEVELOPMENT
 @discussion Requires edit licence.
 @abstract move a part in the score
 @param partIndex index of part to move
 @param destPartIndex destination partindex of part after reinsert
 @return true if it succeeded
 */
-(bool)movePart:(int)partIndex dest:(int)destPartIndex;

/*!
 @method deletePart:
 @warning EDITING API IN DEVELOPMENT
 @discussion Requires edit licence.
 @abstract delete a part from the score
 @param partIndex the index [0..] of a part in the score
 @return true if it succeeded
 */
-(bool)deletePart:(int)partIndex;

/** Access fields in SSDirectionType corresponding to direction-type in XML **/

/*!
 @method staffIndexForDirection:
 @warning EDITING API IN DEVELOPMENT
 @abstract get the staff index for a direction-type
 @param dirType a direction-type
 @return the staff index in the part - 0 is top staff in part
 */
-(int)staffIndexForDirection:(SSDirectionType* _Nonnull)dirType;

/*!
 @method directiveForDirection:
 @warning EDITING API IN DEVELOPMENT
 @abstract get the directive flag for a direction-type (if set the left of the direction words is aligned with the left of the time signature, else with the first note)
 @param dirType a direction-type
 @return the directive flag
 */
-(bool)directiveForDirection:(SSDirectionType* _Nonnull)dirType;

/*!
 @method placementForDirection:
 @warning EDITING API IN DEVELOPMENT
 @abstract get the placement for a direction-type
 @param dirType a direction-type
 @return the placement for the direction
 */
-(enum sscore_placement_e)placementForDirection:(SSDirectionType* _Nonnull)dirType;

/*!
 @method wordsForDirection:
 @warning EDITING API IN DEVELOPMENT
 @abstract get the text string for a direction-type
 @param dirType a direction-type
 @return the text string
 */
-(NSString* _Nullable)wordsForDirection:(SSDirectionTypeWords* _Nonnull)dirType;

/*!
 @method boldForDirection:
 @warning EDITING API IN DEVELOPMENT
 @abstract get the bold flag for a direction-type
 @param dirType a direction-type
 @return the bold flag
 */
-(bool)boldForDirection:(SSDirectionTypeWords* _Nonnull)dirType;

/*!
 @method italicForDirection:
 @warning EDITING API IN DEVELOPMENT
 @abstract get the italic flag in a direction-type
 @param dirType a direction-type
 @return the italic flag
 */
-(bool)italicForDirection:(SSDirectionTypeWords* _Nonnull)dirType;

/*!
 @method pointSizeForDirection:
 @warning EDITING API IN DEVELOPMENT
 @abstract get the font point size for a direction words element
 @param dirType a direction-type words element
 @return the font point size
 */
-(CGFloat)pointSizeForDirection:(SSDirectionTypeWords* _Nonnull)dirType;

/*!
 @method setDirection:words:
 @warning EDITING API IN DEVELOPMENT
 @discussion Requires edit licence.
 @abstract modify the text in SSDirectionTypeWords
 @param dirType a direction-type
 @param words the text
 @return true if success
 */
-(bool)setDirection:(SSDirectionTypeWords* _Nonnull)dirType words:(NSString * _Nonnull)words;

/*!
 @method setDirection:words:pointSize:bold:italic:
 @warning EDITING API IN DEVELOPMENT
 @discussion Requires edit licence.
 @abstract modify the text and font in SSDirectionTypeWords
 @param dirType a direction-type
 @param words the text
 @param pointSize the point size of the font .. 0 is default
 @param bold the bold flag
 @param italic the italic flag
 @return true if success
 */
-(bool)setDirection:(SSDirectionTypeWords* _Nonnull)dirType words:(NSString * _Nonnull)words pointSize:(CGFloat)pointSize bold:(bool)bold italic:(bool)italic;

/*!
 @method addLyricInPart:barIndex:staffIndex:lyricLineIndex:text:linkNextSyllable:
 @warning EDITING API IN DEVELOPMENT
 @discussion Requires edit licence.
 @abstract add a lyric
 @param partIndex the part
 @param barIndex the bar
 @param staffIndex the staff
 @param lyricLineIndex the lyric line
 @param note_h the unique handle of the note to receive the lyric
 @param text the lyric text
 @param linkNextSyllable true to link this syllable to the next lyric with dashes
 @return true if success
 */
-(bool)addLyricInPart:(int)partIndex
			 barIndex:(int)barIndex
		   staffIndex:(int)staffIndex
	   lyricLineIndex:(int)lyricLineIndex
			   note_h:(sscore_item_handle)note_h
				 text:(NSString* _Nonnull)text
	 linkNextSyllable:(bool)linkNextSyllable;

/*!
 @method modifyLyric:text:linkNextSyllable:
 @warning EDITING API IN DEVELOPMENT
 @discussion Requires edit licence.
 @abstract modify a lyric
 @param lyric a lyric from lyricForComponent
 @param text the new text or "" for no change
 @param linkNextSyllable true to link this syllable to the next lyric with dashes
 @return true if success
 */
-(bool)modifyLyric:(SSLyric* _Nonnull)lyric text:(NSString* _Nonnull)text linkNextSyllable:(bool)linkNextSyllable;

/*!
 @method insertBars:numBars:withRest
 @warning EDITING API IN DEVELOPMENT
 @abstract insert empty bars into score
 @param target the target at which the bars are inserted
 @param numBars the number of bars to insert
 @param withRest true to insert a whole-bar rest in each bar
 @discussion Requires edit licence.
 */
-(bool)insertBars:(SSTargetLocation* _Nonnull)target numBars:(int)numBars withRest:(bool)withRest;

/*!
 @method removeEmptyBars
 @warning EDITING API IN DEVELOPMENT
 @abstract remove any empty bars in score
 @discussion Requires edit licence.
 */
-(void)removeEmptyBars;

/*!
 @method setSystemLayout:
 @warning EDITING API IN DEVELOPMENT
 @abstract specify new system bars ie each bar at the left of each system (except the first bar which is understood to be on the left)
 @discussion Requires edit licence.
 @param newSystemBars the index of each bar at the system left to add a system break to
 @return true if score changed
 */
-(bool)setSystemLayout:(NSSet<NSNumber*>* _Nonnull)newSystemBars;

/*!
 @method canCorrectAnyIdentifiedXmlErrors
 @warning EDITING API IN DEVELOPMENT
 @abstract return true if this score has xml errors which can be corrected with correctIdentifiedXmlErrors
 */
-(bool)canCorrectAnyIdentifiedXmlErrors;

/*!
 @method correctIdentifiedXmlErrors
 @warning EDITING API IN DEVELOPMENT
 @abstract test for certain xml errors and perhaps modify the xml to prevent certain tests from failing
  */
-(void)correctIdentifiedXmlErrors;

/** change handler **/

/*!
 @method addChangeHandler:
 @abstract add a handler to be called on each state change (edit, undo or redo)
 @discussion the handler is called with the old and new state. These states can be compared
 so if the change affects only one bar then perhaps only that needs to be updated.
 All parts of the UI which show a representation of any part of the score should use a handler
 to force a redraw so everything updates automatically on a state change
 @param handler the change handler
 @return a unique id for the handler to be used as an argument to removeChangeHandler
 */
-(sscore_changeHandler_id)addChangeHandler:(id<ScoreChangeHandler> _Nonnull)handler;

/*!
 @method removeChangeHandler:
 @abstract remove the change handler added with addChangeHandler:
 @param handlerId the id returned from addChangeHandler
 */
-(void)removeChangeHandler:(sscore_changeHandler_id)handlerId;

// internal use
@property (readonly) const sscore_libkeytype * _Nonnull key;

@end

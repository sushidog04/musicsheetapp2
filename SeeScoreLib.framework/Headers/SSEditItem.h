//
//  SSEditItem.h
//  SeeScoreLib
//
//  Copyright (c) 2016 Dolphin Computing Ltd. All rights reserved.
//
// No warranty is made as to the suitability of this for any purpose
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#include "SSEditType.h"

/*!
 @header SSEditItem.h
 @abstract info about an object in the score which can be edited
 */

/*!
 @interface SSEditItem
 @abstract wrapper for info about an object in the score which can be edited
 */
@interface SSEditItem : NSObject

/*!
 @property editType
 @abstract the SSEditType for the item
 */
@property (readonly) SSEditType * _Nonnull editType;

/*!
 @property baseType
 @abstract the base type
 */
-(enum sscore_edit_baseType)baseType;

/*!
 @property partIndex
 @abstract the index of the part containing the item
 */
@property (readonly) int partIndex;

/*!
 @property barIndex
 @abstract the index of the bar containing the item
 */
@property (readonly) int barIndex;

/*!
 @property rawitem
 @abstract the sscore_edit_item
 */
@property (readonly) const sscore_edit_item * _Nonnull rawitem;

@property (readonly) const sscore_edit_type rawType;

@property (readonly) bool isBezierControl;

-(instancetype _Nonnull)init NS_UNAVAILABLE;
@end

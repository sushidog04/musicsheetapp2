//
//  SSTargetLocation.h
//  SeeScoreLib
//
//  Copyright (c) 2016 Dolphin Computing Ltd. All rights reserved.
//
// No warranty is made as to the suitability of this for any purpose
//

#import <CoreGraphics/CoreGraphics.h>
#include "sscore_edit.h"

@class SSSystem;

/*!
 @header SSTargetLocation.h
 @abstract a logical insertion location in the score
 */

/*!
 @interface SSTargetLocation
 @abstract a logical insertion location in the score defined by part, bar, closest notehead and type of item to insert
 */
@interface SSTargetLocation : NSObject

/*!
 @method initWithTarget:
 @abstract initializer
 @param target the target
 */
-(instancetype _Nonnull)initWithTarget:(const sscore_edit_targetLocation* _Nonnull)target;

/*!
 @property partIndex
 @abstract the part index of the target
 */
@property (readonly) int partIndex;

/*!
 @property barIndex
 @abstract the bar index of the target
 */
@property (readonly) int barIndex;

/*!
 @property staffIndex
 @abstract the staff index (0 is top or only staff in part, 1 is bottom) of the target
 */
@property (readonly) int staffIndex;

/*!
 @property nearestNoteHandle
 @abstract the unique item handle of the nearest note to the target
 */
@property (readonly) sscore_item_handle nearestNoteHandle;

/*!
 @property staffLineSpaceIndex
 @abstract index of staff line/space. Bottom line = 0, bottom space = 1, bottom ledger = -2 etc
 */
@property (readonly) int staffLineSpaceIndex;

/*!
 @property systemXLocation
 @abstract x position of target wrt system
 */
@property (readonly) enum sscore_system_xlocation_e systemXLocation;

/*!
 @property rawtarget
 @abstract the low level C-API object
 */
@property (readonly) const sscore_edit_targetLocation * _Nonnull rawtarget;

-(instancetype  _Nonnull)init NS_UNAVAILABLE;
@end
